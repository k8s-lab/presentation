﻿% Développement de micro-services sur Kubernetes
% gang of Four
% 28 03 2018

## Introduction

### But du lab

 * Premier contact avec k8s
 * Outillage pour le développeur
 * Utilisation de k8s
 * Bonnes pratiques

<aside class="notes">
les commandes de base
les outils pour vous faciliter la vie
comment utiliser k8s pour simplifier le setup d'une machine de dev
</aside>

### Nous

 * @jflabbe - Développeur Energiency
 * @glivron - Développeur Advalo
 * @jgoblet - Développeur Advalo
 * @srevereault - Consultant Zenika

<aside class="notes">
chacun notre tour
</aside>
### Setup initial

SSID : LABK8S / mdp : breizhcamp

http://cluster:30413

* Télécharger l'image Docker
* Télécharger les sources
* Importer l'image Docker

```
$ docker load -i breizhcamp_betting.tgz
```

<aside class="notes">
Sylvain
</aside>

### Intérêts

 * Orchestration très complète
 * Devient le standard
 * Richesse fonctionnelle
 * Richesse d'applications
<aside class="notes">
Sylvain
</aside>

## Kubernetes - Vision logique
![](img/k8s-archi.png)

<aside class="notes">
pod: 1 ou plusieurs conteneurs apportant une fonctionnalité
replicatSet: nombre de pod à faire tourner
deployment: gestion du cycle de vie des replicatSet, rolling upgrade
Service: comment accéder au pods, lb, dns
</aside>

## Kubernetes - Autres concepts

* Labels
* Persistent Volume / Persistent Volume Claim
* Statefulsets
* Jobs / Cronjobs
* Configmaps / secrets
* Health Checks

## Kubernetes

<aside class="notes">
Sylvain
</aside>

### Option de déploiement

 * Minikube
 * GKE, AKS, EKS
 * Kubeadm, Kops, Kubespray...
 * "The hard way"
<aside class="notes">
Sylvain
</aside>

## Kubectl
###
Se placer dans le répertoire des sources

```
$ docker run -ti -v $PWD:/data jeanfrancoislabbe/breizhcamp_betting:all bash
```

```
$ kubectl get all

$ kubectl create namespace bzh0

$ kubectl get namespaces

$ kubectl config set-context $(kubectl config current-context) --namespace=bzh0
```

## Kubectl
###
```
$ kubectl run qotm --image=datawire/qotm:1.3 --port=5000

$ kubectl get deployments

$ kubectl get replicasets

$ kubectl get pods
```

## Kubectl
###
```
$ kubectl port-forward $POD 5000 &

$ kubectl logs -f $POD

$ curl -v http://127.0.0.1:5000
```

## Kubectl
###
```
$ kubectl scale --replicas=3 deployment/qotm

$ watch kubectl get pods

$ kubectl delete --all pods

$ watch kubectl get pods
```

## Kubectl
###
```
$ kubectl delete deployment qotm

$ watch kubectl get pods
```

<aside class="notes">
Guillaume
</aside>

## Dans la vrai vie... fichiers YAML
###
* deployments
* services
* pvc
* ...

### Kompose

Génère les fichiers k8s à partir d'un `docker-compose.yml`

```
$ kompose convert -f docker-compose.yml
INFO Kubernetes file "mongo-service.yaml"     created
INFO Kubernetes file "web-service.yaml"       created
INFO Kubernetes file "mongo-deployment.yaml"  created
INFO Kubernetes file "web-deployment.yaml"    created
```

## Helm
###
* k8s package manager
* applications pré-configurées
* variabilisation des applications
* beaucoup d'applications packagées
[https://github.com/kubernetes/charts](https://github.com/kubernetes/charts)

<aside class="notes">
Sylvain

pratique pour faire un switch dev / production
possible de générer un chart à partir de docker-compose avec Kompose
</aside>

### Kompose

Génère un Chart Helm à partir d'un `docker-compose.yml`

```
$ kompose convert -f docker-compose.yml -c
INFO Kubernetes file "mongo-service.yaml" created 
INFO Kubernetes file "web-service.yaml" created   
INFO Kubernetes file "mongo-deployment.yaml" created 
INFO Kubernetes file "web-deployment.yaml" created 
INFO chart created in "./docker-compose/"

```

## Helm
###
Déploiement de notre Chart
```
$ cd /data
$ helm install ./docker-compose/
```

## Helm - Variabilisation
###
Editer le fichier `docker-compose/values.yaml`
```
service:
  port: 5000
```
Editer le fichier `docker-compose/templates/web-service.yaml`
```
spec:
  ports:
  - name: "3000"
    port: {{ .Values.service.port }}
```

## Helm
###
Nettoyage...
```
$ helm list #Relever le nom de la release

$ helm delete --purge $NomDeLaRelease
```

Déploiement de notre Chart variabilisé
```
$ helm install ./docker-compose/
```

## Helm
###
Nettoyage...
```
$ helm list #Relever le nom de la release

$ helm delete --purge $NomDeLaRelease
```

Déploiement de notre Chart variabilisé en surchargeant le port
```
$ helm install ./docker-compose/ --set service.port=8000
```

## Helm
###
Nettoyage...
```
$ helm list #Relever le nom de la release

$ helm delete --purge $NomDeLaRelease
```

## Draft
###

`$ draft init ; draft create ; draft up`

* Auto-génération de Chart Helm
* Création et push des images
* Déploiement dans un cluster k8s

## Telepresence
###
<aside class="notes">
Jeff
</aside>
![](img/telepresence.svg)

[https://www.telepresence.io (open source)](https://www.telepresence.io)

### Contexte
* Développement de micro-services
* Beaucoup de dépendances
* Setup difficile sur un pc de développeur
* Difficile de mettre à disposition des données

###
![](img/1-kubernetes.png){height=500px}

<aside class="notes">
</aside>

### Utilité

* Développement env == Production env
* Simplification du setup pour un développeur
* Tester facilement des micro-services

<aside class="notes">
Jeff

Env de dev similaire à l'env de production.
Reproduire le mieux possible l'env de production en dev

Simplification du setup pour un développeur, pas besoin de faire marcher tous les services sur l'ordinateur.

</aside>

### Principe

* Proxy bidirectionnel entre kubernetes et l'ordinateur

###
![](img/1-kubernetes.png){height=500px}

<aside class="notes">
</aside>

###
![](img/3-kubernetes.png){height=500px}

<aside class="notes">
</aside>

###
![](img/4-kubernetes.png){height=500px}

<aside class="notes">
</aside>

### Support
<aside class="notes">
Jeff

Linux provides far more capabilities (mount namespaces, bind mounts, network namespaces) than macOS.
</aside>

* Linux
* MacOs (limité)
* Windows avec WSL(Windows Subsystem for Linux)


### TP

### Setup

```shell
  cd /usr/src/app

  kubectl apply -f k8s/

  kubectl describe service betting

    #NodePort:                 3000  30144/TCP
```

Pour accéder à l’application utilisez **`192.168.1.120:30144`**

<aside class="notes">
Jeff
</aside>

### Run telepresence
```shell
telepresence --namespace bzh0 \
             --swap-deployment betting \
             --expose 3000 --method=inject-tcp \
             --run rails s

kubectl delete -f k8s/

```

Le trafic doit arriver dans votre conteneur sur votre machine.

<aside class="notes">
Jeff
</aside>

### TP

## Bonnes pratiques
### Health check
<aside class="notes">
Julien
</aside>
Application alive
```
livenessProbe:
    httpGet:
        path: /healthcheck
        port: 8080
    initialDelaySeconds: 10
    periodSeconds: 5
    successThreshold: 1
    failureThreshold: 3
```

###
Application prête
```
readinessProbe:
    httpGet:
        path: /ready
        port: 8080
    initialDelaySeconds: 2
    periodSeconds: 5
```

### Config maps
<aside class="notes">
Sylvain
</aside>
* Création de fichier
* Variables d'environnement

```
apiVersion: v1
kind: ConfigMap
metadata:
  name: live-app-config
  namespace: default
data:
  special.how: very
  special.type: charm
  app.yml: |
    default:
      log:
        mode: 'verbose'
  ...

```
###
Utilisation dans vos déployements
```
volumes:
    - name: config
        configMap:
            name: live-app-config
containers:
    - ...
     volumeMounts:
        - name: config
          mountPath: /app/config/

```
### Config maps
```
env:
    - name: VARIABLE1
        valueFrom:
          configMapKeyRef:
            name: my-env
            key: VARIABLE1

```

### Secrets
<aside class="notes">
Guillaume
</aside>
```
env:
    - name: SECRET_USERNAME
        valueFrom:
          secretKeyRef:
            name: mysecret
            key: username
    - name: SECRET_PASSWORD
        valueFrom:
          secretKeyRef:
            name: mysecret
            key: password
```
### Contraintes
cpu / mem
<aside class="notes">
Jeff
</aside>
Permet la maitrise des ressources de vos apps
```
resources:
    limits:
        memory: 2Gi
        cpu: 1000m
    requests:
        memory: 500Mi
        cpu: 250m
```
### Pv / Pvc
<aside class="notes">
Sylvain
</aside>
### Infra as code
<aside class="notes">
Sylvain
</aside>
